package zadanie1;

public class TicketGenerator {

    private static final TicketGenerator instance = new TicketGenerator();
    public static TicketGenerator getInstance() {
        return instance;
    }
    private TicketGenerator(){
    }

    private int counter = 0;

    public int getCounter(){
        return counter++;
    }

}
