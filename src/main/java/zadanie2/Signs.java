package zadanie2;

public enum Signs {
    PLUS('+'), MINUS('-'), MULTIPLY('*'), DIVIDE('/');

    private char sign;

    Signs(char sign) {
        this.sign = sign;
    }

    public char getSign() {
        return sign;
    }

    public void setSign(char sign) {
        this.sign = sign;
    }
}
