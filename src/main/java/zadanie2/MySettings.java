package zadanie2;

public class MySettings {

    private static MySettings instance;
    public static MySettings getInstance(){
        if(instance== null){
            instance = new MySettings();
            ConfigurationReader cr = new ConfigurationReader();
            cr.readFile();
        }
        return instance;
    }

    private MySettings(){

    }

    private int zakresLiczbyPierwszej;
    private int zakresLiczbyDrugiej;
    private String dostepneDzialania;
    private int iloscRund;

    public int getZakresLiczbyPierwszej() {
        return zakresLiczbyPierwszej;
    }

    public void setZakresLiczbyPierwszej(int zakresLiczbyPierwszej) {
        this.zakresLiczbyPierwszej = zakresLiczbyPierwszej;
    }

    public int getZakresLiczbyDrugiej() {
        return zakresLiczbyDrugiej;
    }

    public void setZakresLiczbyDrugiej(int zakresLiczbyDrugiej) {
        this.zakresLiczbyDrugiej = zakresLiczbyDrugiej;
    }

    public String getDostepneDzialania() {
        return dostepneDzialania;
    }

    public void setDostepneDzialania(String dostepneDzialania) {
        this.dostepneDzialania = dostepneDzialania;
    }

    public int getIloscRund() {
        return iloscRund;
    }

    public void setIloscRund(int iloscRund) {
        this.iloscRund = iloscRund;
    }
}
