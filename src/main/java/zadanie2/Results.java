package zadanie2;

public class Results {
    private static final Results instance = new Results();
    public static Results getInstance(){
        return instance;
    }
    private Results(){

    }
    private int goodAnswears = 0;

    public void incrementGoodAnswears(){
        goodAnswears++;
    }

    public int getGoodAnswears(){
        return goodAnswears;
    }
}
