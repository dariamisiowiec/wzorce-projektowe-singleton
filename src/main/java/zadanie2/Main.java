package zadanie2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Game game = new Game();
        String answearInString;
        int i = 1;


        // czytasz linie tu
        // while ( game.notEnded())

        // czytaj linie
        // przekaz ja do game
        //

        while(!game.hasEnded(i)) {
            Round round = new Round();
            System.out.println("Runda " + i + ":");
            round.askQuestion();
            round.getTheAnswearFromPlayer(sc.nextLine());
            round.parseAnswear();
            round.checkIfAnswearIsCorrect();
            i++;
        }
        game.showStatistics();

    }
}
