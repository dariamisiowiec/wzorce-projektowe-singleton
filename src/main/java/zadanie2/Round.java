package zadanie2;

import java.util.Random;

public class Round {

    private int number1;
    private int number2;
    private String sign;
    private String answearInString;
    private int answear;


    private int generateNumberInRange(int range){
        Random random = new Random();
        return random.nextInt(range);
    }

    private String generateSign(){
        Random random = new Random();
        String[] signs = MySettings.getInstance().getDostepneDzialania().split("");
        int whichSign = random.nextInt(signs.length);
        return signs[whichSign];
    }

    public void askQuestion(){
        number1 = generateNumberInRange(MySettings.getInstance().getZakresLiczbyPierwszej());
        do {
            number2 = generateNumberInRange(MySettings.getInstance().getZakresLiczbyDrugiej());
            sign = generateSign();
        } while (number2 == 0 && sign.equals("/"));
        System.out.println("Ile wynosi " + number1 + " " + sign + " " + number2 + "?");

    }

    public void getTheAnswearFromPlayer(String answearInString){
        this.answearInString = answearInString;
    }

    public void parseAnswear() {
        try {
            answear = Integer.parseInt(answearInString);
        } catch (NumberFormatException nfe) {
            System.out.println("To nie jest liczba");
        }
    }

    public void checkIfAnswearIsCorrect() {
        if (sign.equals("+")) {
            if (number1 + number2 == answear) {
                afterGoodAnswear();
            }else
                System.out.println("Źle :( Poprawna odpowiedź to " + (number1+number2));
        } else if (sign.equals("-")) {
            if (number1 - number2 == answear) {
                afterGoodAnswear();
            }else
                System.out.println("Źle :( Poprawna odpowiedź to " + (number1-number2));
        } else if (sign.equals("/")) {
            if (number1/number2 == answear) {
                afterGoodAnswear();
            }else
                System.out.println("Źle :( Poprawna odpowiedź to " + (number1/number2));
        } else if (sign.equals("*")) {
            if (number1 * number2 == answear) {
                afterGoodAnswear();
            }else
                System.out.println("Źle :( Poprawna odpowiedź to " + (number1*number2));
        }
    }

    private void afterGoodAnswear(){
        System.out.println("Dobrze :)");
        Results.getInstance().incrementGoodAnswears();
    }
}
