package zadanie2;

public class Game {

    public boolean hasEnded(int i){
        return (i>MySettings.getInstance().getIloscRund());
    }

    public void showStatistics(){
        System.out.println("Liczba poprawnych odpowiedzi: " + Results.getInstance().getGoodAnswears() +
                "/" + MySettings.getInstance().getIloscRund());
    }

}
