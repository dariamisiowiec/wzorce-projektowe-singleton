package zadanie2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ConfigurationReader {

        public void readFile(){
        File file = new File("C:\\Users\\Anna1\\Projekty\\singleton\\src\\main\\java\\zadanie2\\plik.txt");
        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new FileReader(file));
            String line;
            String[] splitedLine;
            line = reader.readLine();
            splitedLine = line.split("=");
            MySettings.getInstance().setZakresLiczbyPierwszej(Integer.valueOf(splitedLine[1]));
            line = reader.readLine();
            splitedLine = line.split("=");
            MySettings.getInstance().setZakresLiczbyDrugiej(Integer.valueOf(splitedLine[1]));
            line = reader.readLine();
            splitedLine = line.split("=");
            MySettings.getInstance().setDostepneDzialania(splitedLine[1]);
            line = reader.readLine();
            splitedLine = line.split("=");
            MySettings.getInstance().setIloscRund(Integer.valueOf(splitedLine[1]));
        }catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
